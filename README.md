# notmyidea-sps #

A fork of notmyidea-cms, for the Johns Hopkins University [Society of Physics Students](http://pha.jhu.edu/groups/sps).

The screenshot is no longer accurate.

This should eventually go upstream to the pelican-themes repository, but it needs polishing.

## Screenshot ##

![screenshot](screenshot.png)
